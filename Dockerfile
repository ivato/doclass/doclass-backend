FROM python:3.6

WORKDIR /app

COPY ./requirements.txt ./
RUN pip install -r requirements.txt

COPY ./ /app

EXPOSE 5000
CMD [ ".", ".env" ]
CMD [ "flask", "run", "-h", "0.0.0.0"]
