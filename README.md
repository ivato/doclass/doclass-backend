# Doclass Backend

## Prerequisites
* Linux Distribution: tested sucessfully on Ubuntu 18.04
* Python 3
* Virtualenv
* MongoDB

### Used technologies

* [Flask](https://flask.palletsprojects.com/) is a lightweight and extensible web framework.
* [Flask-RESTPlus](https://flask-restplus.readthedocs.io/en/stable/) is an extension for Flask that adds support for quickly building REST APIs. It provides a coherent collection of decorators and tools to describe your API and expose its documentation properly (using [Swagger](https://swagger.io/)).
* [flask_kvsession](https://pythonhosted.org/Flask-KVSession/) is library to python interact with key-value databases, it permits that a single key session in Client is used to retrieve data in Redis.
* [Mongoengine](http://mongoengine.org/) is a Python Object-Document Mapper for working with [MongoDB](https://www.mongodb.com/)
* [NLTK](https://www.nltk.org/) is a platform for building Python programs to work with human language data. It provides a list of stopwords, used to preprocess text.
* [Numpy](https://numpy.org) in addition to being used indirectly by Scikit-learn, it was also used directly to [serialize](https://numpy.org/doc/stable/reference/generated/numpy.save.html) the preprocessing cache.
* [Pandas](https://pandas.pydata.org/) is a fast, powerful, flexible and easy to use open source data analysis and manipulation tool, built on top of the Python programming language. A very useful feature of this technology is the lazy loading of dataset in compressed CSV format (gzipped), that could alleviate the memory overhead on the backend.  
* [Python](https://www.python.org/) is the general-purpose language used for backend development, one of the most widely used for machine learning.
* [Redis](https://redis.io/) is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker. In this project, it was used to store session data.
* [Scikit-learn](https://scikit-learn.org/) is a python library for simple and efficient tools for predictive data analysis. In this project, we use it to transform textual data to vectors (vectorization) and machine learning.
* [SciPy](https://www.scipy.org/) is a Python-based ecosystem of open-source software for mathematics, science, and engineering. In this project, we use it to transform a [sparse to dense matrix](https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.todense.html)

### How to test performance
Performance can be tested with performance.py script. Examples:

Test performance with dataset id 1 with 25 random selected documents and generate 100 predictions, calculate average after 5 rounds:
```
$ export PYTHONPATH=.
$ python app/test/performance.py 1 25 100 5
...
FINAL RESULTS:
average load dataset time: 1.1220115661621093
average preprocess time: 0.7977714538574219
average prediction times: 0.437362003326416
average train times: 3.05063747215271
average preprocess cache times: 0.034260892868041994
```



Test performance dataset id 1 with 50 random selected documents and generate 200 predictions, calculate average after 5 rounds.

```
$ export PYTHONPATH=.
$ python app/test/performance.py 1 50 200 5
...
FINAL RESULTS:
average load dataset time: 2.8483877658843992
average preprocess time: 0.6196404457092285
average prediction times: 1.0798302173614502
average train times: 3.713043529510498
average preprocess cache times: 0.029098653793334962
```

## Installation

1. Clone the project

```bash
git clone https://gitlab.com/ivato/textgraph/doclass-backend
```

2. Create a virtual environment

```bash 
virtualenv --python=python3 doclass-backend/
```

3. Activate the virtual environment

```bash
cd doclass-backend
source bin/activate
```

4. Install python modules

```bash
pip install -r requirements.txt
```

## Install example dataset

1. Download and install 20\_newsgroups dataset

```bash
mkdir data
cd data
wget http://qwone.com/~jason/20Newsgroups/20news-19997.tar.gz
tar xvfz 20news-19997.tar.gz
```

2. Create sample database
```bash
cd ..
mongo < db/create-20newsgroups-db.js
```

## Executing the API

1. Execute:
```bash
cd scripts
./doclass-backend-ctl start
```

2. Check if it is running:
```bash
./doclass-backend-ctl status
```

3. Open your browser at http://localhost:5000
