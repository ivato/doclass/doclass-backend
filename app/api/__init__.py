from flask_restplus import Api, fields, Namespace, Model
# from .repository import repo_api
# from .collection import collection_api
from .label import label_api, labelset_api
from .doc import doc_api
# from .classification import classification_api
from .dataset import dataset_api
from .session import session_api


def get_api():
    doclass_api = Api(version='1.0', title='Doclass',
                      description='Document Classification')

    # doclass_api.add_namespace(repo_api)
    # doclass_api.add_namespace(collection_api)
    print('label api loading...')
    doclass_api.add_namespace(label_api)
    print('label api loaded')
    # doclass_api.add_namespace(labelset_api)
    doclass_api.add_namespace(doc_api)
    # doclass_api.add_namespace(classification_api)
    doclass_api.add_namespace(dataset_api)
    doclass_api.add_namespace(session_api)
    return doclass_api
