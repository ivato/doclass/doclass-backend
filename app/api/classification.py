from flask_restplus import Api, Resource, fields, abort, Namespace, Model
from flask import Response
from app.db.classification import Classification as ClassificationDB
from app.db.label import Label as LabelDB
from app.api.label import label
import json

classification_api = Namespace('classification', description="Document classification")

classification = classification_api.model('classification', {
    '_id': fields.Integer(unique=True, description='Classification ID'),
    'document': fields.Integer(description='Classified document id'),
    'value': fields.Float(description='Value of classification certainty'),
    'label': fields.Integer(description='Label applied to this document'),
    'classifier_type': fields.String(description='Type of classifier: human, algorithm, computer rule, etc'),
    'classifier_name': fields.String(description='Name of human or algorithm used to classify'),
    'datetime': fields.String(description='UTC Timestamp when classification was done')
})

# ground truth / prediction relation
gtp_relation = classification_api.model('gtp_relation', {
    'groundtruth_id': fields.Integer(description='Groundtruth (GT)label id'),
    'prediction_id': fields.Integer(description='Prediction (P) label id'),
    'groundtruth_name': fields.String(description='Groundtruth label name'),
    'prediction_name': fields.String(description='Classification label name'),
    'quantity': fields.Integer(description='Number of ocurrencies of pair GT/P')
})


@classification_api.route("/")
class Classification(Resource):
    @classification_api.doc('list all classifications')
    @classification_api.marshal_list_with(classification)
    def get(self):
        """
        return all classifications
        """
        classifications = ClassificationDB.list()
        return classifications


@classification_api.route("/<int:id>")
class Classification(Resource):
    @classification_api.doc('return classification identified by id')
    @classification_api.marshal_with(classification, code=201)
    def get(self, id):
        """
        return classification details identified by id
        """
        classification = ClassificationDB.get_by_id(id)
        return json.loads(classification.to_json()), 201


@classification_api.route("/collection/<int:collection_id>")
class Classification(Resource):
    @classification_api.doc('list all classifications from collection')
    @classification_api.marshal_list_with(classification)
    def get(self, collection_id):
        """
        return all classifications from collection
        """
        classifications = ClassificationDB.filter_classifications(collection_id=collection_id)
        return classifications


@classification_api.route("/repo/<int:repo_id>")
class Classification(Resource):
    @classification_api.doc('list all classifications from repository')
    @classification_api.marshal_list_with(classification)
    def get(self, repo_id):
        """
        list all classification from repository
        """
        classifications = ClassificationDB.filter_classifications(repo_id=repo_id)
        return classifications


@classification_api.route("/doc/<int:doc_id>")
class Classification(Resource):
    @classification_api.doc('list all classifications from a document')
    @classification_api.marshal_list_with(classification)
    def get(self, doc_id):
        """
        return all classifications from a document
        """
        classifications = ClassificationDB.filter_classifications(doc_id=doc_id)
        return classifications


@classification_api.route("/gtp_relation")
class Classification(Resource):
    @classification_api.doc('ground truth/prediction relation')
    @classification_api.marshal_list_with(gtp_relation)
    def get(self):
        """
        return ground truth/prediction relation
        """
        gtp = ClassificationDB.gtp_relation()
        gtpr = []
        for pair in gtp.keys():
            relation = {}
            relation['groundtruth_id'] = pair[0]
            relation['prediction_id'] = pair[1]
            relation['groundtruth_name'] = \
                LabelDB.get_by_id(pair[0])['full_name']
            relation['prediction_name'] = \
                LabelDB.get_by_id(pair[1])['full_name']
            relation['quantity'] = gtp[pair]
            gtpr.append(relation)
        return gtpr


@classification_api.route("/gtp_relation/<string:set>/<int:set_id>/after/<string:dt_after>/before/<string:dt_before>")
class Classification(Resource):
    @classification_api.doc('ground truth/prediction relation')
    @classification_api.marshal_list_with(gtp_relation)
    def get(self, set, set_id, dt_after, dt_before):
        """
        return ground truth/prediction relation from a given set of documents (collection, repo or doc) classified after/before a given datetime
        """
        collection_id = None
        repo_id = None
        doc_id = None
        if set == 'collection':
            collection_id = set_id
        elif set == 'repo':
            repo_id = set_id
        elif set == 'doc_id':
            doc_id = set_id
        dt_start = dt_after
        dt_end = dt_before
        gtp = ClassificationDB.gtp_relation(collection_id, repo_id, \
                                            doc_id, dt_start, dt_end)
        gtpr = []
        for pair in gtp.keys():
            relation = {}
            relation['groundtruth_id'] = pair[0]
            relation['prediction_id'] = pair[1]
            relation['groundtruth_name'] = \
                LabelDB.get_by_id(pair[0])['full_name']
            relation['prediction_name'] = \
                LabelDB.get_by_id(pair[1])['full_name']
            relation['quantity'] = gtp[pair]
            gtpr.append(relation)
        return gtpr
