from flask_restplus import Api, Resource, fields, abort, Namespace, Model
from app.db import Collection as CollectionDB
from app.db import Repository as RepoDB
import json
from app.api.repository import repo

collection_api = Namespace('collection', description="Collection operations")
collection = collection_api.model('Collection', {
    '_id': fields.Integer(unique=True, description='Collection ID'),
    'name': fields.String(required=True, unique=True, description='Collection name'),
    'description': fields.String(description='Collection description'),
    'repo_list': fields.List(fields.Integer, description='list of repositories id')
})


@collection_api.route("/")
class CollectionsList(Resource):
    @collection_api.doc('list all collections')
    @collection_api.marshal_list_with(collection)
    def get(self):
        """
        return all collections
        """
        collections = CollectionDB.list()
        return collections

    @collection_api.doc('create collection')
    @collection_api.expect(collection)
    @collection_api.marshal_with(collection, code=201)
    def post(self):
        """
        create a new collection to the list
        """
        try:
            c = CollectionDB(**collection_api.payload)
            c.save()
        except Exception as e:
            abort(400, "Error creating collection: " + str(e))
        return c, 201

    @collection_api.doc('update a selected collection')
    @collection_api.expect(collection)
    def put(self):
        """
        update a selected collection
        """
        try:
            req = collection_api.payload
            id = req['_id']
            del req['_id']
            c = CollectionDB.objects(_id=id)[0]
            for k in c:
                if k == 'repo_list' or k == '_id':
                    continue
                c[k] = req[k]
            c.save()
        except Exception as e:
            abort(400, "Error updating collection: " + str(e))
        return json.loads(c.to_json()), 201


@collection_api.route("/<int:id>")
class Collection(Resource):
    @collection_api.doc('display a collection\'s details')
    def get(self, id):
        """
        display a collection's details
        """
        try:
            c = CollectionDB.get_by_id(id)
        except Exception as e:
            abort(400, "Error accessing collection: " + str(e))
        return json.loads(c.to_json()), 201

    @collection_api.doc('delete collection identified by id')
    def delete(self, id):
        """
        delete a collection identified by id
        """
        try:
            c = CollectionDB.get_by_id(id)
            c.delete()
        except Exception as e:
            abort(400, "Error deleting repository: " + str(e))
        return id, 201

    @collection_api.doc('reload collection identified by id')
    def patch(self, id):
        """
        reload collection identified by id
      """
        # try:
        # except:
        return id, 201


'''
@collection_api.route("/<int:id>/repo/list")
class CollectionListRepo(Resource):
    @collection_api.doc('list repositories of this collection')
    def list_repo(self, id):
          """
          list repositories of this collection
          """
          try:
            c = CollectionDB.get_by_id(id)
          except Exception as e:
            abort(400, "Error listing repositories: "+str(e))
          return c.repo_list, 201
'''


@collection_api.route("/<int:id_collection>/repo_list")
class CollectionRepoList(Resource):
    @collection_api.doc('list repository in collection')
    @collection_api.marshal_list_with(repo)
    def get(self, id_collection):
        """
      list repository in collection
      """
        try:
            c = CollectionDB.get_by_id(id_collection)
        except Exception as e:
            abort(400, 'Error on list repositories: ' + str(e))
        return c.list_repo(), 201


@collection_api.route("/<int:id_collection>/<string:op>/<int:id_repo>")
class CollectionRepo(Resource):
    @collection_api.doc('remove or add repository in collection')
    def get(self, id_collection, op, id_repo):
        """
        remove or add repository in collection
        """
        try:
            c = CollectionDB.get_by_id(id_collection)
            r = RepoDB.get_by_id(id_repo)
            if op == 'add':
                c.add_repo(r)
            elif op == 'remove':
                c.remove_repo(r._id)
            else:
                raise Exception('Error: operation not recognized: ' + op)
        except Exception as e:
            abort(400, 'Error on ' + op + ' in collection: ' + str(e))
        return json.loads(c.to_json()), 201
