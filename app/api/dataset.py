from flask_restplus import Api, Resource, fields, abort, Namespace, Model
from app.db import Dataset as DatasetDB
import json

dataset_api = Namespace('dataset', description="Dataset operations")
dataset = dataset_api.model('Dataset', {
    '_id': fields.Integer(unique=True, description='Dataset ID'),
    'name': fields.String(required=True, unique=True, description='Dataset name'),
    'url': fields.String(required=True, unique=True, description='HTTP URL Address')
})


@dataset_api.route("/")
class DatasetList(Resource):
    @dataset_api.doc('list all datasets')
    @dataset_api.marshal_list_with(dataset)
    def get(self):
        """
        return all datasets
        """
        datasets = DatasetDB.list()
        return datasets

    @dataset_api.doc('create dataset')
    @dataset_api.expect(dataset)
    @dataset_api.marshal_with(dataset, code=201)
    def post(self):
        """
        create a new dataset to the list
        """
        try:
            d = DatasetDB(**dataset_api.payload)
            d.save()
        except Exception as e:
            abort(400, "Error creating dataset: " + str(e))
        return d, 201

    @dataset_api.doc('update a selected dataset')
    @dataset_api.expect(dataset)
    def put(self):
        """
        update a selected dataset
        """
        try:
            req = dataset_api.payload
            id = req['_id']
            del req['_id']
            d = DatasetDB.objects(_id=id)[0]
            for k in d:
                if k == '_id':
                    continue
                d[k] = req[k]
            d.save()
        except Exception as e:
            abort(400, "Error updating dataset: " + str(e))
        return json.loads(d.to_json()), 201


@dataset_api.route("/<int:id>/select_doc/<int:doc_id>")
class DatasetSelectDoc(Resource):
    @dataset_api.doc('select a document to classify')
    def get(self, id, doc_id):
        """
        select a document to classify
        """
        try:
            d = DatasetDB.get_by_id(id)
            id, label_ind, unlab_ind = d.select_doc(doc_id)
        except Exception as e:
            abort(400, "Error accessing dataset: " + str(e))
        return id, 201


@dataset_api.route("/<int:id>")
class Dataset(Resource):
    @dataset_api.doc('display a dataset\'s details')
    def get(self, id):
        """
        display a dataset's details
        """
        try:
            d = DatasetDB.get_by_id(id)
        except Exception as e:
            abort(400, "Error accessing dataset: " + str(e))
        return json.loads(d.to_json()), 201

    @dataset_api.doc('delete dataset identified by id')
    def delete(self, id):
        """
        delete a dataset identified by id
        """
        try:
            d = DatasetDB.get_by_id(id)
            d.delete()
        except Exception as e:
            abort(400, "Error deleting dataset: " + str(e))
        return id, 201
