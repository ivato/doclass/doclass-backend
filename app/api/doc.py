from flask_restplus import Api, Resource, fields, abort, Namespace, Model
from flask import Response
from app.db.doc import Doc as DocDB
from app.db.label import Label as LabelDB
from app.api.label import label
import json


def output_txt(data, code, headers=None):
    resp = Response(data, mimetype='text/plain', headers=headers)
    resp.status_code = code
    return resp


doc_api = Namespace('document', description="Document operations")

# classification = doc_api.model('classification', {
#    '_id':  fields.Integer(unique=True, description='Classification ID'),
#    'value': fields.Float(description='Value of classification certainty'),
#    'label': fields.Integer(description='Label applied to this document'),
#    'classifier_type': fields.String(description='Type of classifier: human, algorithm, computer rule, etc'),
#    'classifier_name': fields.String(description='Name of human or algorithm used to classify'),
#    'datetime': fields.String(description='UTC Timestamp when classification was done')
# })

document = doc_api.model('document', {
    '_id': fields.Integer(unique=True, description='Document ID'),
    # 'filename': fields.String(required=True, description='Document filename'),
    'key': fields.String(required=True, description='Document key'),
    # 'repo': fields.Integer(description='Repository'),
    'dataset': fields.Integer(description='Origin dataset'),
    'labels': fields.List(fields.Integer(description='Labels applied to this document')),
    'predictions': fields.List(fields.Integer(description='Labels predicted to this document')),
})


# doc_classification = doc_api.model('doc_classification', {
#    '_id': fields.Integer(unique=True, description='Document ID'),
#    'filename': fields.String(required=True, description='Document filename'),
#    'alg_value': fields.Float(description='Value of classification certainty by algorithm'),
#    'alg_label': fields.Integer(description='Label applied to this document by algorithm'),
#    'alg_classifier_type': fields.String(description='Type of classifier: human, algorithm, comoputer rule, etc'),
#    'alg_classifier_name': fields.String(description='Name of human or algorithm used to classify  by algorithm'),
#    'alg_datetime': fields.String(description='UTC Timestamp when classification was done  by algorithm'),
#    'hum_value': fields.Float(description='Value of classification certainty by algorithm'),
#    'hum_label': fields.Integer(description='Label applied to this document by algorithm'),
#    'hum_classifier_type': fields.String(description='Type of classifier: human, algorithm, comoputer rule, etc'),
#    'hum_classifier_name': fields.String(description='Name of human or algorithm used to classify  by algorithm'),
#    'hum_datetime': fields.String(description='UTC Timestamp when classification was done  by algorithm')    
# })


@doc_api.route("/list/<int:index>/<int:size>/from/<int:dataset_id>")
class DocumentList(Resource):
    @doc_api.doc('list documents from <index> with <size> length from <dataset_id>')
    @doc_api.marshal_list_with(document)
    def get(self, index, size, dataset_id):
        """
        list all documents from <index> with <size> length, from <dataset_id>
        """
        documents = DocDB.docs_from_dataset(dataset_id, index, size)
        return documents


@doc_api.route("/dataset/<int:dataset_id>")
class DocumentsFromDataset(Resource):
    @doc_api.doc('list all documents from dataset')
    @doc_api.marshal_list_with(document)
    def get(self, dataset_id):
        """
        list all documents from dataset
        """
        documents = DocDB.docs_from_dataset(dataset_id)
        return documents


@doc_api.route("/dataset/<int:dataset_id>/count")
class DocumentsCountFromDataset(Resource):
    @doc_api.doc('count documents from dataset')
    def get(self, dataset_id):
        """
        return number of documents in dataset
        """
        return DocDB.count(dataset_id=dataset_id)


# @doc_api.route("/repo/<int:repo_id>")
# class DocumentsFromRepo(Resource):
#    @doc_api.doc('list all documents from repository')
#    @doc_api.marshal_list_with(document)    
#    def get(self, repo_id):
#        """
#        list all documents from repository
#        """
#        documents = DocDB.docs_from_repo(repo_id)
#        return documents

# @doc_api.route("/classifications")
# class DocumentClassification(Resource):
#    @doc_api.doc('list all classifications')
#    @doc_api.marshal_list_with(doc_classification)    
#    def get(self):
#        """
#        return all classifications
#        """
#        classifications = DocDB.get_classifications()
#        return classifications

# @doc_api.route("/classifications/doc/<int:doc_id>")
# class DocumentClassificationDoc(Resource):
#    @doc_api.doc('list all classifications from a document')
#    @doc_api.marshal_list_with(doc_classification)    
#    def get(self, doc_id):
#        """
#        return all classifications from a document
#        """
#        classifications = DocDB.get_classifications(doc_id=doc_id)
#        return classifications

# @doc_api.route("/classifications/repo/<int:repo_id>")
# class DocumentClassificationRepo(Resource):
#    @doc_api.doc('list all classifications from repository')
#    @doc_api.marshal_list_with(doc_classification)    
#    def get(self, repo_id):
#        """
#        return all classifications from repository
#        """
#        classifications = DocDB.get_classifications(repo_id=repo_id)
#        return classifications

# @doc_api.route("/classifications/collection/<int:collection_id>")
# class DocumentClassificationCollection(Resource):
#    @doc_api.doc('list all classifications from collection')
#    @doc_api.marshal_list_with(doc_classification)    
#    def get(self, collection_id):
#        """
#        return all classifications from collection
#        """
#        classifications = DocDB.get_classifications(collection_id=collection_id)
#        return classifications

# @doc_api.route("/classifications/gtp_relation")
# class DocumentClassificationGPT(Resource):
#    @doc_api.doc('ground truth/prediction relation')
# @doc_api.marshal_list_with(doc_classification)
#    def get(self):
#        """
#        return ground truth/prediction relation
#        """
#        gtpr = DocDB.get_groundtruth_prediction_relation()
#        return gtpr

@doc_api.route("/<int:id>")
class Document(Resource):
    @doc_api.marshal_with(document, code=201)
    @doc_api.doc('return document details identified by id')
    def get(self, id):
        """
        return document details identified by id
        """
        doc = DocDB.get_by_id(id)
        return json.loads(doc.to_json()), 201


@doc_api.route("/content/<int:id>")
class DocumentContent(Resource):
    @doc_api.doc('read text from document identified by id')
    def get(self, id):
        """
        return text from document identified by id
        """
        doc = DocDB.get_by_id(id)
        return output_txt(doc.read(), 200)


@doc_api.route("/<int:id_doc>/<string:op>/<int:id_label>")
class DocumentLabel(Resource):
    @doc_api.doc('remove or apply a label to document, use op=remove or op=add')
    def get(self, id_doc, op, id_label):
        """
        remove or apply a label to document
        """
        try:
            print('removing l.id: ' + str(id_label))
            d = DocDB.get_by_id(id_doc)
            l = LabelDB.get_by_id(id_label)
            if op == 'add':
                d.add_label(l)
            elif op == 'remove':
                d.remove_label(id_label)
            else:
                raise Exception('Error: operation not recognized: ' + op)
        except Exception as e:
            abort(400, 'Error on ' + op + ' in document: ' + str(e))
        return json.loads(d.to_json()), 202


@doc_api.route("/<int:id>/labels")
class DocumentLabelData(Resource):
    @doc_api.marshal_list_with(label)
    @doc_api.doc('get label data from a document identified by id')
    def get(self, id):
        """
        get label data from a document identified by id
        """
        try:
            d = DocDB.get_by_id(id)
        except Exception as e:
            abort(400, 'Error on retrieving labels in document ' + str(id) + ': ' + str(e))
        return [json.loads(l.to_json()) for l in d.labels]
