from flask_restplus import Api, Resource, fields, abort, Namespace, Model
from app.db.label import Label as LabelDB
from app.db.label import LabelSet as LabelSetDB
import json

label_api = Namespace('label', description="Label operations")
# label_options = label_api.model('LabelOptions', {
#  'alias': fields.String(description='Label alias'),
#  'color': fields.String(description='Label color')  
# })

label = label_api.model('Label', {
    '_id': fields.Integer(unique=True, description='Label ID'),
    'name': fields.String(required=True, description='Label name'),
    'full_name': fields.String(required=True, unique=True, description='Label fullname'),
    'description': fields.String(description='Label description'),
    'parent': fields.Integer(description='Parent label'),
    'label_set': fields.Integer(description='Labelset which label is part of'),
    'property': fields.String(description='Property of this label (metalabel or not)'),
    # 'options': fields.List(fields.String, description='Options for this label')
    'color': fields.String(description='Color used to present this label'),
    'alias': fields.String(description='Alias used to show this label')
})


@label_api.route("/")
class LabelList(Resource):
    @label_api.doc('list all labels')
    @label_api.marshal_list_with(label)
    def get(self):
        """
        return all labels
        """
        labels = LabelDB.list()
        return LabelDB.list()

    @label_api.doc('create label')
    @label_api.expect(label)
    @label_api.marshal_with(label, code=201)
    def post(self):
        """
        create a new label to the list
        """
        try:
            l = LabelDB(**label_api.payload)
            l.save()
        except Exception as e:
            abort(400, "Error creating label: " + str(e))
        return json.loads(l.to_json()), 201

    @label_api.doc('update a selected label')
    @label_api.expect(label)
    def put(self):
        """
        update a selected label
        """
        try:
            req = label_api.payload
            id = req['_id']
            del req['_id']
            l = LabelDB.objects(_id=id)[0]
            for k in l:
                if k == '_id' or k == 'valid_transitions':
                    continue
                elif k == 'parent' and k in req:
                    p = LabelDB.get_by_id(req[k])
                    l[k] = p
                elif k == 'label_set' and k in req:
                    ls = LabelSetDB.get_by_id(req[k])
                    l[k] = ls
                else:
                    l[k] = req[k]
            l.save()
        except Exception as e:
            abort(400, "Error updating label: " + str(e))
        return json.loads(l.to_json()), 201


@label_api.route("/<int:id>")
class Label(Resource):
    @label_api.doc('display a label\'s details')
    def get(self, id):
        """
        display a labels's details
        """
        try:
            l = LabelDB.get_by_id(id)
        except Exception as e:
            abort(400, "Error accessing label: " + str(e))
        return json.loads(l.to_json()), 201

    @label_api.doc('delete label identified by id')
    def delete(self, id):
        """
        delete a label identified by id
        """
        try:
            l = LabelDB.get_by_id(id)
            l.delete()
        except Exception as e:
            abort(400, "Error deleting label: " + str(e))
        return id, 201


labelset_api = Namespace('labelset', description="Labelset operations")
labelset = labelset_api.model('LabelSet', {
    '_id': fields.Integer(unique=True, description='LabelSet ID'),
    'name': fields.String(required=True, description='LabelSet name'),
    'full_name': fields.String(required=True, unique=True, description='LabelSet fullname'),
    'description': fields.String(description='LabelSet description'),
    'labels': fields.List(fields.Integer, description='List of labels in this set'),
    'parent': fields.Integer(description='Parent labelset'),
    'property': fields.String(description='Property of this labelset (meta, any_of or one_of)'),
    'properties': fields.List(fields.String, description='List of properties of this set')
})


@labelset_api.route("/")
class LabelSetList(Resource):
    @labelset_api.doc('list all labelsets')
    @labelset_api.marshal_list_with(labelset)
    def get(self):
        """
        return all labelsets
        """
        labelsets = LabelSetDB.list()
        return LabelSetDB.list()

    @labelset_api.doc('create labelset')
    @labelset_api.expect(labelset)
    @labelset_api.marshal_with(labelset, code=201)
    def post(self):
        """
        create a new labelset to the list
        """
        try:
            ls = LabelSetDB(**labelset_api.payload)
            ls.save()
        except Exception as e:
            abort(400, "Error creating labelset: " + str(e))
        return json.loads(ls.to_json()), 201

    @labelset_api.doc('update a selected labelset')
    @labelset_api.expect(labelset)
    def put(self):
        """
        update a selected labelset
        """
        try:
            req = labelset_api.payload
            id = req['_id']
            del req['_id']
            ls = LabelSetDB.objects(_id=id)[0]
            for k in ls:
                if k == '_id' or k == 'labels':
                    continue
                elif k == 'parent' and k in req:
                    p = LabelSetDB.get_by_id(req[k])
                    ls[k] = p
                else:
                    ls[k] = req[k]
            ls.save()
        except Exception as e:
            abort(400, "Error updating labelset: " + str(e))
        return json.loads(ls.to_json()), 201


@labelset_api.route("/<int:id>")
class LabelSet(Resource):
    @labelset_api.doc('display a labelset\'s details')
    def get(self, id):
        """
        display a labelset's details
        """
        try:
            ls = LabelSetDB.get_by_id(id)
        except Exception as e:
            abort(400, "Error accessing labelset: " + str(e))
        return json.loads(ls.to_json()), 201

    @label_api.doc('delete labelset identified by id')
    def delete(self, id):
        """
        delete a labelset identified by id
        """
        try:
            ls = LabelSetDB.get_by_id(id)
            ls.delete()
        except Exception as e:
            abort(400, "Error deleting labelset: " + str(e))
        return id, 201


@labelset_api.route("/<int:id>/labels")
class LabelsetLabels(Resource):
    @labelset_api.doc('list labels in a labelset identified by id')
    @labelset_api.marshal_list_with(label)
    def get(self, id):
        """
        list labels in a labelset identified by id
        """
        try:
            result = []
            # all labels
            if id == 0:
                result = LabelDB.list()
            else:
                ls = LabelSetDB.get_by_id(id)
                # for l in ls.labels:
                #  ld = json.loads(l.to_json())                  
                #  ld['options'] = json.loads(ls.get_options(l).to_json())
                #  result.append(ld)
                result = [json.loads(l.to_json()) for l in ls.labels]
        except Exception as e:
            abort(400, "Error accessing labelset: " + str(e))
        return result
