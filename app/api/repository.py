from app.db import Repository as RepoDB
from flask_restplus import fields, Resource, abort, Namespace, Model
import json

repo_api = Namespace('repo', description="Repository operations")
repo = repo_api.model('Repo', {
    '_id': fields.Integer(unique=True, description='Collection ID'),
    'relative_path': fields.String(required=True, unique=True, description='Relative path of repo in filesystem')
})


@repo_api.route("/")
class RepoList(Resource):

    @repo_api.doc('list all repositories')
    @repo_api.marshal_list_with(repo)
    def get(self):
        """
        return all repositories
        """
        return RepoDB.list()

    @repo_api.doc('create repository')
    @repo_api.expect(repo)
    @repo_api.marshal_with(repo, code=201)
    def post(self):
        """
        create a new repository
        """
        try:
            r = RepoDB(**repo_api.payload)
            r.create()
        except Exception as e:
            abort(400, "Error creating repository: " + str(e))
        return r, 201

    @repo_api.doc('update a selected repository')
    @repo_api.expect(repo)
    def put(self):
        try:
            req = repo_api.payload
            id = req['_id']
            del req['_id']
            r = RepoDB.objects(_id=id)[0]
            r.update(req)
        except Exception as e:
            abort(400, "Error updating repository: " + str(e))
        return json.loads(r.to_json()), 201


@repo_api.route("/<int:id>")
class Repo(Resource):

    @repo_api.doc('delete repository identified by id')
    def delete(self, id):
        """
        delete a repository identified by id
        """
        try:
            r = RepoDB.get_by_id(id)
            r.remove()
        except Exception as e:
            abort(400, "Error deleting repository: " + str(e))
        return id, 201

    @repo_api.doc('return repository identified by id')
    @repo_api.marshal_with(repo, code=201)
    def get(self, id):
        """
        return repository identified by id
        """
        try:
            r = RepoDB.get_by_id(id)
        except Exception as e:
            abort(400, "Error retrieving repository by id " + str(id) + ":" + str(e))
        return r, 201

# @repo_api.route("/<int:id>/listdocs")
