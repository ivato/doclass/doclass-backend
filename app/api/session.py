from flask_restplus import Api, Resource, fields, abort, Namespace, Model
from flask import session as session_flask
from app.db.session import Session as SessionDB
from app.db.dataset import Dataset as DatasetDB
from app.api.doc import document

import json

session_api = Namespace('session', description="Session operations")
session = session_api.model('Session', {
    '_id': fields.Integer(unique=True, description='Session ID'),
    'email': fields.String(required=True, description='Email'),
    'selected_docs': fields.String(description='Document selected'),
    'available_docs': fields.String(description='Document available to select'),
    'dataset': fields.Integer(description='Dataset in operation'),
    'accuracy': fields.Float(description='Current accuracy of model'),
    'mean_confidence': fields.Float(description='Current mean confidence of model'),
    'min_confidence': fields.Float(description='Current min confidence of an example of model'),
    'created_at': fields.DateTime(description='Time when session was opened'),
    'updated_at': fields.DateTime(description='Time when session was updated'),
    'status': fields.String(description='Status of this session: init, opened, closed'),
    'file_model': fields.String(description='File path of model where model is saved')
})

login = session_api.model('Login', {
    'email': fields.String,
    'session_id': fields.Integer
})

session_opening = session_api.model('SessionOpening', {
    'email': fields.String,
    'dataset_id': fields.Integer
})

docs_query = session_api.model('DocsQuery', {
    'order_by': fields.String,
    'direction': fields.String,
    'start': fields.Integer,
    'size': fields.Integer,
    'status': fields.String
})


@session_api.route("/")
class Session(Resource):
    @session_api.doc('get session data')
    def get(self):
        """
        get session data
        """
        s = session_flask.get('session')
        if s is None:
            return json.loads('{}')
        else:
            return json.loads(s.to_json()), 201

    @session_api.doc('open session with email and dataset_id')
    @session_api.expect(session_opening)
    def post(self):
        """
        open session with email and dataset_id
        """
        req = session_api.payload
        s = SessionDB()
        dataset_id = req['dataset_id']
        dataset = DatasetDB.get_by_id(dataset_id)
        email = req['email']
        s.open(dataset, email)
        session_flask['session'] = s
        return json.loads(s.to_json()), 201

    @session_api.doc('close session')
    def delete(self):
        """
        close session
        """
        try:
            s = session_flask['session']
            session_id = s._id
            session_flask['session'] = None
        except:
            raise Exception('could not close session')
        return json.loads('{"removed_ok": "' + str(session_id) + '"}'), 201


@session_api.route("/reopen")
class SessionReopen(Resource):
    @session_api.doc('reopen session with email and session id')
    @session_api.expect(login)
    def post(self):
        """
        reopen session with email and session id
        """
        try:
            req = session_api.payload
            session_id = req['session_id']
            s = SessionDB.get_by_id(session_id)
            email = req['email']
            if email != s.email:
                raise Exception('informed email doesn\'t match')
            session_flask['session'] = s
        except Exception as e:
            abort(400, "Error: " + str(e))
        return json.loads('{"email": "' + email + '", "session_id": ' + str(session_id) + '}'), 201


@session_api.route("/docs/<string:status>/<int:start>/<int:size>")
class SessionDocs(Resource):
    # @session_api.marshal_list_with(document)
    @session_api.doc("return documents in session whith a informed status: selected or available")
    def get(self, status, start, size):
        """
    return documents in session whith a informed status: selected or available
    """
        try:
            docs = []
            s = session_flask['session']
            if s is None:
                raise Exception('session is not opened')
            if status == 'selected':
                docs = s.selected_docs[start:start + size]
            elif status == 'available':
                docs = s.available_docs[start:start + size]
            else:
                raise Exception('document status not exists')
        except Exception as e:
            abort(400, "Error: " + str(e))
        return [json.loads(d.to_json()) for d in s.predictions(docs)]


@session_api.route("/docs/query")
class SessionDocsQuery(Resource):
    @session_api.expect(docs_query)
    @session_api.doc("query documents in session ordered by a field")
    def post(self):
        """
    query documents in session ordered by a field
    """
        try:
            docs = []
            req = session_api.payload
            order_by = req['order_by']
            direction = req['direction']
            start = req['start']
            size = req['size']
            status = req['status']
            s = session_flask['session']
            if s is None:
                raise Exception('session is not opened')
            if status == 'selected':
                docs = s.selected_docs
            elif status == 'available':
                docs = s.available_docs
            else:
                raise Exception('document status not exists')
            docs = s.predictions(docs)
            reverse = True if direction == 'desc' else False
            order = {'key': lambda doc: doc.key,
                     'confidence': lambda doc: doc.predictions[0].confidence,
                     'id': lambda doc: doc._id,
                     'label': lambda doc: doc.predictions[0].label.value}
            if order_by is not None:
                docs = [d for d in sorted(docs, key=order[order_by], reverse=reverse)]
        except Exception as e:
            abort(400, "Error: " + str(e))
        return [json.loads(d.to_json()) for d in docs[start:start + size]]


@session_api.route("/doc/select/<int:doc_id>")
class SessionDocSelect(Resource):
    @session_api.doc("select a document as example to be labeled and train the model")
    def get(self, doc_id):
        """
    select a document as example to be labeled and train the model
    """
        try:
            s = session_flask.get('session')
            if s is None:
                raise Exception('session is not opened')
            r = s.select_doc(doc_id)
            if isinstance(r, SessionDB):
                session_flask['session'] = r
            else:
                raise Exception('session changed status: ' + str(r))
        except Exception as e:
            abort(400, "Exception: " + str(e))
        return json.loads(s.to_json())


@session_api.route("/list/<string:email>")
class SessionList(Resource):
    @session_api.doc("list sessions associated with an email")
    def get(self, email):
        """
    list sessions associated with an email
    """
        try:
            sessions = SessionDB.list(email=email)
        except Exception as e:
            abort(400, "Error: " + str(e))
        return sessions


@session_api.route("/log/<int:session_id>")
class SessionLogs(Resource):
    @session_api.doc("return logs for a session identified with an id")
    def get(self, session_id):
        """
    return logs for a session identified with an id
    """
        try:
            s = SessionDB.get_by_id(session_id)
            logs = [json.loads(l.to_json()) for l in s.logs]
        except Exception as e:
            abort(400, "Error: " + str(e))
        return logs
