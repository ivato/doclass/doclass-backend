from mongoengine import *
import os
import sys
import json
from .doc import Doc
from .collection import Collection
from .repository import Repository
from .classification import Classification
from .dataset import Dataset
from app.main.config import config_by_name

config = config_by_name['default']

db = config.MONGODB_DATABASE
md_host = config.MONGODB_HOST
md_user = config.MONGODB_USER
md_password = config.MONGODB_PASSWORD
md_port = int(config.MONGODB_PORT)

try:
    if md_user is None:
        db_uri = 'mongodb://' + md_host + '/' + db
    else:
        db_uri = 'mongodb+srv://' + md_user + ':' + md_password + '@' + md_host + '/' + db + '?retryWrites=true&w' \
                                                                                             '=majority '
    connect(host=db_uri)
except Exception as e:
    print('Could not connect to database: ' + str(e))
    sys.exit(-1)
