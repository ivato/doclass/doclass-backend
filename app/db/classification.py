from mongoengine import *
from app.db.label import Label
from app.db.doc import Doc
from app.db.collection import Collection
import json
from datetime import datetime
from mongoengine.queryset.visitor import Q
import dateutil.parser


class Classification(Document):
    classifier_types = {
        'human': 'human classifier',
        'computer': 'computer rule classifier',
        'algorithm': 'algorithm classifier'
    }
    _id = SequenceField(primary_key=True)
    document = ReferenceField('Doc', required=True)
    value = FloatField(min_value=-1, max_value=1)
    label = ReferenceField(Label, required=True)
    classifier_type = StringField(choices=classifier_types.keys())
    classifier_name = StringField()
    datetime = DateTimeField()
    labelset_cache = {}
    labelname_cache = {}

    @staticmethod
    def list():
        return [json.loads(c.to_json()) for c in Classification.objects]

    @staticmethod
    def get_by_id(id):
        return Classification.objects(_id=id)[0]

    @staticmethod
    def filter_classifications(collection_id=None, repo_id=None, \
                               doc_id=None, classifier_type='algorithm', \
                               dt_start=None, dt_end=None):
        l = []
        repo_list = None
        conditions = [{'classifications': {'$ne': []}}]
        # datetime isoformat, ex.: 2019-07-24T05:55:40.283Z
        if dt_start is not None and dt_end is not None:
            dt_s = dateutil.parser.parse(dt_start)
            print('dt_start: ' + dt_start)
            dt_e = dateutil.parser.parse(dt_end)
            print('dt_end: ' + dt_end)
            conditions.append({'datetime': {'$gte': dt_s}})
            conditions.append({'datetime': {'$lte': dt_e}})
        conditions.append({'classifier_type': classifier_type})
        pipeline = [
            {
                '$lookup': {
                    'from': 'classification',
                    'localField': '_id',
                    'foreignField': 'document',
                    'as': 'classifications'
                }
            }, {
                '$unwind': '$classifications'
            }, {
                '$project': {
                    '_id': '$classifications._id',
                    'document': '$_id',
                    'repo': '$repo',
                    'filename': '$filename',
                    'value': '$classifications.value',
                    'gt_labels': '$labels',
                    'label': '$classifications.label',
                    'classifier_type': '$classifications.classifier_type',
                    'classifier_name': '$classifications.classifier_name',
                    'datetime': '$classifications.datetime'
                }
            }, {
                '$match': {
                    '$and': conditions
                }
            }
        ]
        query = None
        if doc_id is None:
            if repo_id is not None:
                repo_list = [repo_id]
            elif collection_id is not None:
                repo_list = Collection.get_by_id(collection_id).list_repo()
            if repo_list is not None:
                query = Q(repo__in=repo_list)
        else:
            query = Q(_id=doc_id)
        for d in Doc.objects(query).aggregate(*pipeline):
            l.append(d)
        return l

    @staticmethod
    def labelset(label_id):
        if label_id not in Classification.labelset_cache:
            Classification.labelset_cache[label_id] = \
                Label.get_by_id(label_id).label_set._id
        return Classification.labelset_cache[label_id]

    @staticmethod
    def labelname(label_id):
        if label_id not in Classification.labelname_cache:
            Classification.labelname_cache[label_id] = \
                Label.get_by_id(label_id).full_name
        return Classification.labelname_cache[label_id]

    # return label from label list with same labelset 
    @staticmethod
    def match_labelset(label, labels):
        if label in labels:
            return (label, label)
        else:
            i = list(map(Classification.labelset, labels)) \
                .index(Classification.labelset(label))
            return (label, labels[i])

    @staticmethod
    def gtp_relation(collection_id=None, repo_id=None, \
                     doc_id=None, classifier_type='algorithm', \
                     dt_start=None, dt_end=None):
        sum_gtp = {}
        for c in Classification.filter_classifications( \
                collection_id, repo_id, doc_id, \
                classifier_type, dt_start, dt_end \
                ):
            cl = Classification.labelname
            pair = Classification.match_labelset( \
                c['label'], c['gt_labels'])
            if pair not in sum_gtp:
                sum_gtp[pair] = 0
            else:
                sum_gtp[pair] = sum_gtp[pair] + 1
        return sum_gtp

    '''
    @staticmethod
    def get_groundtruth_prediction_relation():
      gtp = {}
      gtp['yh_yc'] = (2,4)
      gtp['nh_nc'] = (3,5)
      gtp['uh_uc'] = (7,8)
      gtp['yh_nc'] = (2,5)
      gtp['yh_uc'] = (2,8)
      gtp['nh_yc'] = (3,4)
      gtp['nh_uc'] = (3,8)
      gtp['uh_yc'] = (7,4)
      gtp['uh_nc'] = (7,5)
      pgt = {}
      pgt[(2,4)] = 'yh_yc'
      pgt[(3,5)] = 'nh_nc' 
      pgt[(7,8)] = 'uh_uc' 
      pgt[(2,5)] = 'yh_nc' 
      pgt[(2,8)] = 'yh_uc' 
      pgt[(3,4)] = 'nh_yc' 
      pgt[(3,8)] = 'nh_uc' 
      pgt[(7,4)] = 'uh_yc' 
      pgt[(7,5)] = 'uh_nc'
      sum_gtp = {}
      sum_gtp['yh_yc'] = 0
      sum_gtp['nh_nc'] = 0
      sum_gtp['uh_uc'] = 0
      sum_gtp['yh_nc'] = 0
      sum_gtp['yh_uc'] = 0
      sum_gtp['nh_yc'] = 0
      sum_gtp['nh_uc'] = 0
      sum_gtp['uh_yc'] = 0
      sum_gtp['uh_nc'] = 0
      for d in Doc.objects():
        dc = {}
        cs = d.first_classification_pair()
        hl = cs[0]['label']['_id']
        cl = cs[1]['label']['_id']
        hcl = pgt[(hl,cl)]
        sum_gtp[hcl] = sum_gtp[hcl] + 1
      return sum_gtp
  '''
