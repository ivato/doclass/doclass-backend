from mongoengine import *
from app.db.repository import Repository
import json


class Collection(Document):
    _id = SequenceField(primary_key=True)
    name = StringField(required=True, unique=True)
    description = StringField()
    # remove?
    repo_list = ListField(ReferenceField(Repository))

    # properties:
    # url(internet) ou uri (arquivo local ou database)
    # dataframes (pandas, atributo não persistente)

    @staticmethod
    def list():
        return [json.loads(r.to_json()) for r in Collection.objects]

    @staticmethod
    def get_by_id(id):
        return Collection.objects(_id=id)[0]

    def add_repo(self, repo):
        try:
            self.repo_list.append(repo)
            self.save()
        except Exception as e:
            raise Exception('repo ' + repo + ' not added, error: ' + str(e))

    # todo: remove_repo is not working :-\ 
    def remove_repo(self, id):
        try:
            for r in self.repo_list:
                if r._id == id:
                    self.repo_list.remove(r)
                self.save()
                break
        except Exception as e:
            raise Exception('repo ' + id + ' not removed  , error: ' + str(e))

    def list_repo(self):
        try:
            return self.repo_list
        except Exception as e:
            raise Exception('could not list repositories, error: ' + str(e))
