from mongoengine import *
import json
import pandas as pd
from app.db.doc import Doc
from app.db.label import Label
import numpy as np
from app.nlp.activeLearning import ActiveLearning
import os.path
import timeit


class Dataset(Document):
    _id = SequenceField(primary_key=True)
    name = StringField(required=True, unique=True)
    url = StringField(required=True)
    is_loaded = BooleanField(default=False)

    df_time = 0

    preprocess_time = 0
    preprocess_cache_time = 0
    preprocess_cache_hit = False

    is_preprocessed = False
    is_splitted = False
    df = None
    df_loaded = False
    labels_loaded = False
    strategy_loaded = False
    dataset_initiated = False
    model_trained = False
    strategy = None
    alibox = None
    model = None
    accuracy = None
    mean_confidence = None
    min_confidence = None
    stdev_confidence = None
    model = None
    X = None
    y = None
    train_idx = []
    test_idx = []
    label_ind = []
    unlab_ind = []

    def __init__(self, *args, **kwargs):
        super(Document, self).__init__(*args, **kwargs)
        self.preprocess()
        self.data_split()
        self.get_strategy()
        self.dataset_initiated = True

    @staticmethod
    def list():
        return [json.loads(r.to_json()) for r in Dataset.objects]

    @staticmethod
    def get_by_id(id):
        return Dataset.objects(_id=id)[0]

    @staticmethod
    def get_doc_by_key(key):
        return Dataset.objects(key=key)[0]

    def dataframe(self):
        if self.df_loaded:
            return (self.df, 0)
        else:
            a = timeit.time.time()
            self.df = pd.read_csv(self.url)
            b = timeit.time.time()
            self.df_loaded = True
            self.df_time = b - a
            return (self.df, self.df_time)

    def load_labels(self):
        ll = []
        if not self.labels_loaded:
            if not self.df_loaded:
                self.dataframe()
            for c in self.df.columns[2:]:
                cy = c+'.y'
                cn = c+'.n'
                if len(Label.objects(name=cy)) == 0:
                    l = Label()
                    l.name = cy
                    l.description = c+' labeled as yes'
                    l.dataset = self
                    l.value = 1
                    ll.append(l)
                if len(Label.objects(name=cn)) == 0:
                    l1 = Label()
                    l1.name = cn
                    l1.description = c+' labeled as no'
                    l1.dataset = self
                    l1.value = 0
                    ll.append(l1)
            if not len(ll) == 0:
                Label.objects.insert(ll)
            self.labels_loaded = True
        return self.labels_loaded

    def label_names(self):
        if not self.labels_loaded:
            self.load_labels()
        ln = []
        for c in self.df.columns[2:]:
            ln.append(c)
        return ln

    def load_docs(self):
        dl = []
        if not self.is_loaded:
            self.load_labels()
            lns = self.label_names()
            for index, row in self.df.iterrows():
                d = Doc()
                d.key = row['id']
                labels = []
                for ln in lns:
                    if row[ln] == 0:
                        l = Label.objects(name=ln+'.n')[0]
                        labels.append(l)
                    elif row[ln] == 1:
                        l = Label.objects(name=ln+'.y')[0]
                        labels.append(l)
                d.labels = labels
                d.dataset = self
                dl.append(d)
            Doc.objects.insert(dl)
            self.is_loaded = True
            self.save()
        return self.is_loaded

    def reload_docs(self):
        self.is_loaded = False
        self.load_docs()

    def docs(self, start=0, length=0):
        if not self.is_loaded:
            self.load_docs()
        return Doc.docs_from_dataset(self._id, start, length)

    def preprocess(self):
        time = 0
        if not self.df_loaded:
            file_pkl = '/tmp/dataframe.'+str(self._id)+'.pkl'
            if os.path.exists(file_pkl):
                self.df = pd.read_pickle(file_pkl)
            else:
                self.dataframe()
                self.df.to_pickle(file_pkl)
        labels = self.df['ac'].values.tolist()
        corpus = self.df['content'].values.tolist()

        X_npy_file = '/tmp/X.'+str(self._id)+'.npy'
        y_npy_file = '/tmp/y.'+str(self._id)+'.npy'

        if self.is_preprocessed:
            a = timeit.time.time()
            self.X = np.load(X_npy_file)
            self.y = np.load(y_npy_file)
            b = timeit.time.time()
            self.preprocess_cache_time = b-a
            self.preprocess_cache_hit = True
        else:
            a = timeit.time.time()
            self.X, self.y = ActiveLearning.preprocess(labels, corpus)
            b = timeit.time.time()
            self.preprocess_time = b-a
            with open(X_npy_file, 'wb') as file:
                np.save(file, self.X)
                file.close()
            with open(y_npy_file, 'wb') as file:
                np.save(file, self.y)
                file.close()
            self.is_preprocessed =  True
        return (self.X, self.y, self.preprocess_cache_hit) 

    def data_split(self):
        if not self.is_splitted:
            self.train_idx, self.test_idx, self.label_ind, self.unlab_ind = ActiveLearning.data_split(
                self.X, self.y)
            self.is_splitted = True
        return self.train_idx, self.test_idx, self.label_ind, self.unlab_ind

    def get_strategy(self):
        if not self.strategy_loaded:
            self.strategy, self.alibox = ActiveLearning.get_strategy(
                self.X, self.y, self.train_idx)
            self.strategy_loaded = True
        return self.strategy, self.alibox

    def train(self):
        # if not self.dataset_initiated:
        #  self.init()
        self.accuracy, self.score, self.model = \
            ActiveLearning.fit_model(
                self.X, self.y, self.label_ind, self.test_idx, self.alibox)
        self.mean_confidence, self.min_confidence, self.stdev_confidence = \
            ActiveLearning.confidence(self.X, self.model)
        return self.model

    def predict(self, indexes):
        if not model_trained:
            self.train()
        X_indexes = X[indexes]
        preds = ActiveLearning.predict_class(X_indexes, self.model)
        return [(pred, preds[1][i][pred]) for i, pred in enumerate(preds[0])]

    # key to index
    def key_to_ind(self, key):
        return int(self.df[self.df['id'] == key].index.values[0])

    # index to key
    def ind_to_key(self, ind):
        return self.df.loc[ind]['id']

    # id (from object) to ind (dataframe)
    def id_to_ind(self, id):
        return self.key_to_ind(Doc.get_by_id(id)['key'])

    # ind (dataframe) to id (from object)
    def ind_to_id(self, ind):
        return Doc.get_by_key(self.df.loc[ind]['id'])['_id']

    def predict(self, id):
        return ActiveLearning.predict_class(self.X[id], self.model)
