from mongoengine import *

from app.db.label import Label, LabelSet
# from app.db.dataset import Dataset
# from app.db.repository import Repository
# from app.db.collection import Collection
import json
import os
from datetime import datetime


# from mongoengine.queryset.visitor import Q

# class Commit(EmbeddedDocument):
#  hash_id = StringField(unique=True)
#  msg = StringField()
#  date = DateTimeField()
#  author = StringField()

class Prediction(EmbeddedDocument):
    label = ReferenceField(Label)
    confidence = FloatField()


class Doc(Document):
    _id = SequenceField(primary_key=True)
    # id interno do dataset
    key = IntField(required=True)
    # repo = ReferenceField(Repository)
    # groundtruth labels:
    dataset = ReferenceField('Dataset')
    labels = ListField(ReferenceField(Label))
    predictions = ListField(EmbeddedDocumentField(Prediction))

    # @staticmethod
    # def filenames():
    #     return [ o.filename for o in Doc.objects() ]

    # @staticmethod
    # def reload_files_to_db(r):
    #    rfs = r.filenames()
    #    db_fs = Doc.filenames()
    #    try:
    #        for fn in rfs:
    #            if fn not in db_fs:
    #                d = Doc(filename=fn, repo=r)
    #                d.save()
    #    except Exception as e:
    #        raise Exception('Error saving document '+str(d._id)+': '+str(e))

    @staticmethod
    def list(index=0, size=1000):
        # disabled: too slow
        # for r in Repository.objects:
        #    Doc.reload_files_to_db(r)
        return [json.loads(d.to_json()) for d in Doc.objects[index:index + size]]

    @staticmethod
    def count(dataset_id=0):
        if dataset_id == 0:
            return Doc.objects.count()
        else:
            return Doc.objects(dataset=dataset_id).count()

    @staticmethod
    def docs_from_dataset(dataset_id, index=0, size=0):
        if size == 0:
            return [json.loads(d.to_json()) for d in Doc.objects[index:](dataset=dataset_id)]
        else:
            return [json.loads(d.to_json()) for d in Doc.objects[index:index + size](dataset=dataset_id)]

    @staticmethod
    def get_by_id(id):
        return Doc.objects.get(_id=id)

    @staticmethod
    def get_by_key(key):
        return Doc.objects.get(key=key)

    # def is_joinable(self, label):
    #    for l in self.labels:
    #        if not l.is_joinable(label):
    #            return False
    #    return True

    # def first_classification_pair(self):
    #  return (self.classifications[0], self.classifications[1])

    def add_label(self, label):
        try:
            if Label.get_by_id(label._id).property == 'meta':
                raise Exception('Apply meta-label to document is not permitted')
            elif not self.is_joinable(label):
                raise Exception('There is a conflict with another label')
            else:
                self.labels.append(label)
                self.save()
        except Exception as e:
            raise Exception('label ' + str(label._id) + ' not added, error: ' + str(e))
        return True

    def remove_label(self, label_id):
        try:
            for l in self.labels:
                if l._id == label_id:
                    self.labels.remove(l)
                    break
            self.save()
        except Exception as e:
            raise Exception('label ' + str(id) + ' not removed  , error: ' + str(e))
        return True

    def change_label(self, label_orig, label_dest):
        if label_orig not in labels:
            raise Exception('Label ' + label_orig.name + ' was not applied in document')
        elif not self.is_joinable(label_dest):
            raise Exception(
                'Apply label ' + label_dest.name + ' is not valid to this document, there is a conflict with current labels')
        else:
            self.remove_label(label_orig)
            self.add_label(label_dest)
        return label_dest

    def read(self):
        # todo: fazer a leitura pelo datasource (arquivo ou dataframe)
        # fp = os.path.join(self.repo.fullpath(), self.filename)
        # f = open(fp, 'r')
        # return f.read()
        df = self.dataset.dataframe()[0]
        return df.loc[df['id'] == self.key]['content'].to_list()[0]
