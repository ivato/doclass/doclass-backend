from mongoengine import *
import json
from enum import Enum


class Label(Document):
    # property_types = {
    #    'meta': 'used to group labels, not used to classify documents'
    # }
    _id = SequenceField(primary_key=True)
    name = StringField(required=True, unique=True)
    # full_name = StringField(required=True,unique=True)
    description = StringField()
    color = StringField()
    alias = StringField()
    value = IntField()

    # parent = ReferenceField('self')
    # valid_transitions = ListField(ReferenceField('self'))
    # label_set = ReferenceField('LabelSet')
    # property = StringField(choices=property_types.keys())

    @staticmethod
    def list():
        return [json.loads(r.to_json()) for r in Label.objects]

    @staticmethod
    def get_by_id(id):
        return Label.objects(_id=id)[0]

    @staticmethod
    def get_by_prefix_value(prefix, value):
        if value == 0:
            name = prefix + '.n'
        else:
            name = prefix + '.y'
        return Label.objects(name=name)[0]

    def add_valid_transition(self, label):
        if label in self.valid_transitions:
            raise Exception('Error: Label ' + str(label.name) + ' is already a valid transition')
        elif label is self:
            raise Exception('Error: Can\'t include myself as a valid transition')
        else:
            self.valid_transitions.append(label)
        return label

    def remove_valid_transition(self, label):
        if label in self.valid_transitions:
            self.valid_transitions.remove(label)
        else:
            raise Exception('Error: Can\'t remove label ' + str(label.name) + ', since it is not a valid transition')
        return label

    def is_joinable(self, label):
        if self.label_set._id == label.label_set._id and self.label_set.property == 'one_of':
            return False
        else:
            return True


# class LabelOptions(EmbeddedDocument):
#  label = ReferenceField(Label)
#  alias = StringField()
#  color = StringField()

class LabelSet(Document):
    property_types = {
        'meta': 'used to group LabelSets',
        'any_of': 'one or more labels can be applied',
        'one_of': 'only one label can be applied (mutual exclusion)',
        'computer': 'classified by computer',
        'human': 'classified by human',
    }
    _id = SequenceField(primary_key=True)
    name = StringField(required=True)
    full_name = StringField(required=True, unique=True)
    description = StringField()
    labels = ListField(ReferenceField(Label))
    # labels_options = ListField(EmbeddedDocumentField(LabelOptions))
    property = StringField(choices=property_types.keys())
    properties = ListField(StringField(choices=property_types.keys()))
    parent = ReferenceField('self')

    @staticmethod
    def list():
        return [json.loads(r.to_json()) for r in LabelSet.objects]

    @staticmethod
    def get_by_id(id):
        return LabelSet.objects(_id=id)[0]

    # def get_options(self,label):
    #  response = None
    #  for l_options in self.labels_options:
    #    if l_options.label == label:
    #      return l_options
    #  return response

    def add(self, label):
        if label in self.labels:
            raise Exception('Error: Label ' + str(label.name) + ' is already included')
        else:
            self.labels.append(label)
        return self

    def remove(self, label):
        if label not in self.labels:
            raise Exception('Error: Label ' + str(label.name) + ' is not in set')
        else:
            self.labels.remove(label)
        return self

    def labels_for_doc(doc):
        pass
