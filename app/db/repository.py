from mongoengine import *
from git import Repo, InvalidGitRepositoryError
import json
import os
import shutil
from app.main.config import config_by_name


class Repository(Document):
    _id = SequenceField(primary_key=True)
    # relative_path = StringField(required=True,unique=True)
    uri = StringField(required=True, unique=True)
    config = config_by_name['default']
    repos_dir = config.REPOS_DIR
    git_repo = None

    @staticmethod
    def list_valid_repos():
        rd = Repository.repos_dir
        dirs = [name for name in os.listdir(rd) if os.path.isdir(os.path.join(rd, name))]
        valid_repos = []
        for dir in dirs:
            try:
                repo_dir = os.path.join(rd, dir)
                Repo(repo_dir)
                valid_repos.append(dir)
            except InvalidGitRepositoryError:
                pass
        return valid_repos

    @staticmethod
    def list():
        return [json.loads(r.to_json()) for r in Repository.objects]

    @staticmethod
    def get_by_id(id):
        return Repository.objects(_id=id)[0]

    @staticmethod
    def get_trash_dir():
        rd = Repository.repos_dir
        td = os.path.join(rd, '.trash')
        if not os.path.exists(td):
            os.mkdir(td)
        return td

    def create(self):
        try:
            rd = Repository.repos_dir
            new_repo = os.path.join(rd, self.relative_path)
            Repo.init(new_repo)
            self.save()
        except Exception as e:
            raise e

    def get_git_repo(self):
        if self.git_repo is None:
            abs_path = os.path.join(repos_dir, self.relative_path)
            try:
                self.git_repo = Repo(abs_path)
            except InvalidGitRepositoryError as e:
                print('Error: ' + str(e))
        return self.git_repo

    def remove(self):
        td = Repository.get_trash_dir()
        rd = Repository.repos_dir
        gd = os.path.join(rd, self.relative_path)
        gd_new = os.path.join(rd, self.relative_path + '_' + str(self._id))
        try:
            if os.path.exists(gd):
                os.rename(gd, gd_new)
                shutil.move(gd_new, td)
            self.delete()
        except Exception as e:
            raise e

    def update(self, req):
        try:
            for k in self:
                if k in req:
                    if k == 'relative_path' and self[k] != req[k]:
                        rd = Repository.repos_dir
                        old = os.path.join(rd, self[k])
                        new = os.path.join(rd, req[k])
                        os.rename(old, new)
                    self[k] = req[k]
            self.save()
        except Exception as e:
            raise (e)
        return self

    def fullpath(self):
        return os.path.join(self.repos_dir, self.relative_path)

    def filenames(self):
        filelist = []
        try:
            fp = self.fullpath()
            with os.scandir(fp) as it:
                for entry in it:
                    # if entry.name.endswith('.txt') and entry.is_file():
                    if entry.is_file():
                        filelist.append(entry.name)
        except Exception as e:
            raise (e)
        return filelist
