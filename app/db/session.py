from mongoengine import *
from app.db.dataset import Dataset
from app.db.doc import Doc
from app.db.doc import Prediction
from app.db.label import Label
import pickle
from datetime import datetime
import json
from app.nlp.activeLearning import ActiveLearning


class Log(EmbeddedDocument):
    iteration = IntField()
    time = DateTimeField()
    accuracy = FloatField()
    mean_confidence = FloatField()
    min_confidence = FloatField()
    stdev_confidence = FloatField()
    selected_docs = ListField(ReferenceField(Doc))


class Session(Document):
    status_types = {
        'init': 'initial',
        'opened': 'is opened',
        'closed': 'is closed',
        'target_ok': 'goal achieved',
        'limited': 'reached the limit of attempts'
    }
    _id = SequenceField(primary_key=True)
    email = StringField(required=True)
    selected_docs = ListField(ReferenceField(Doc))
    available_docs = ListField(ReferenceField(Doc))
    accuracy = FloatField()
    mean_confidence = FloatField()
    min_confidence = FloatField()
    stdev_confidence = FloatField()
    dataset = ReferenceField(Dataset)
    created_at = DateTimeField()
    updated_at = DateTimeField()
    status = StringField(choices=status_types.keys(), default='init')
    file_model = StringField()
    logs = ListField(EmbeddedDocumentField(Log))
    # mean_confidence_target = FloatField(default=0.70)
    # stdev_confidence_target = FloatField(default=0.1)
    mean_stdev_confidence_target = FloatField(default=0.7)
    iterations_limit = IntField(default=50)
    iterations_min = IntField(default=10)

    @staticmethod
    def list(email=None):
        sessions = []
        if email is None:
            sessions = Session.objects()
        else:
            sessions = Session.objects(email=email)
        return [json.loads(s.to_json()) for s in sessions]

    @staticmethod
    def get_by_id(id):
        return Session.objects(_id=id)[0]

    def save_model(self):
        self.file_model = '/tmp/' + self.email + '.' + str(self._id) + '.pickle'
        with open(self.file_model, 'wb') as file:
            pickle.dump(self.dataset.model, file)
            file.close()

    def retrieve_model(self):
        # file_model = '/tmp/'+self.email+'.'+str(self._id)+'.pickle'
        with open(self.file_model, 'rb') as file:
            self.dataset.model = pickle.load(file)
            file.close()

    # convert list of indexes to list of objects
    def indexes_to_objs(self, indexes):
        k = ''
        obj_list = []
        for i in indexes:
            try:
                # index to key
                k = self.dataset.df.loc[i]['id']
                d = Doc.get_by_key(k)
            except:
                raise (Exception('Document index ' + str(i) + ' not exists'))
            obj_list.append(d)
        return obj_list

    # convert list of objects to list of indexes
    def objs_to_indexes(self, objs):
        indexes = []
        k = ''
        for o in objs:
            try:
                # index to key
                k = o.key
                ind = self.dataset.key_to_ind(k)
            except:
                raise (Exception('Document key ' + str(k) + ' not exists'))
            indexes.append(ind)
        return indexes

    def open(self, dataset, email):
        if self.status == 'init':
            self.dataset = dataset
            self.email = email
            self.dataset.train()
            self.save_model()
            self.created_at = datetime.now()
            self.updated_at = datetime.now()
            self.accuracy = self.dataset.accuracy
            self.mean_confidence = self.dataset.mean_confidence
            self.min_confidence = self.dataset.min_confidence
            self.stdev_confidence = self.dataset.stdev_confidence
            self.selected_docs = self.indexes_to_objs(self.dataset.label_ind)
            self.available_docs = self.indexes_to_objs(self.dataset.unlab_ind)
        else:
            raise Exception('could not open session because is current ' + self.status)
        self.status = 'opened'
        self.save_log()
        self.save()
        return self._id

    def predictions(self, docs):
        if self.dataset.model == None:
            self.dataset.train()
        self.retrieve_model()
        doc_indexes = self.objs_to_indexes(docs)
        docs_with_predictions = []
        preds = self.dataset.predict(doc_indexes)
        for i, p in enumerate(preds[0]):
            d = docs[i]
            pred = Prediction()
            l = Label.get_by_prefix_value('ac', p)
            pred.label = l
            pred.confidence = preds[1][i][p]
            d.predictions = [pred]
            docs_with_predictions.append(d)
        return docs_with_predictions

    # selecting a document with object _id  
    def select_doc(self, id=None):
        try:
            if not self.dataset.model_trained:
                self.dataset.train()
            if self.status == 'limited':
                raise Exception('Limit of iteractions reached')
            if self.status == 'target_ok':
                raise Exception('Goal achieved')
            # update list of selected docs of dataset with session
            self.dataset.label_ind = self.objs_to_indexes(self.selected_docs)
            self.dataset.unlab_ind = self.objs_to_indexes(self.available_docs)
            if id == None:
                # let active learning choose candidate
                ind, self.dataset.label_ind, self.dataset.unlab_ind = \
                    ActiveLearning.select_doc(self.dataset.label_ind, self.dataset.unlab_ind, self.dataset.strategy)
            else:
                # the candidate was labeled by user
                ind = self.dataset.id_to_ind(id)
                self.dataset.unlab_ind.remove(ind)
                self.dataset.label_ind.append(ind)
            # model is trained for every selected doc
            self.dataset.train()
            self.accuracy = self.dataset.accuracy
            self.mean_confidence = self.dataset.mean_confidence
            self.min_confidence = self.dataset.min_confidence
            self.stdev_confidence = self.dataset.stdev_confidence
            self.selected_docs = self.indexes_to_objs(self.dataset.label_ind)
            self.available_docs = self.indexes_to_objs(self.dataset.unlab_ind)
            self.updated_at = datetime.now()
            self.save_model()
            self.save_log()
            iterations = len(self.logs)
            if iterations == self.iterations_limit:
                self.status = 'limited'
            elif iterations > self.iterations_min:
                if (self.mean_confidence - self.stdev_confidence) > \
                        self.mean_stdev_confidence_target:
                    self.status = 'target_ok'
            self.save()
        except Exception as e:
            return e
        return self

    def count_iteration(self):
        return len(self.logs) + 1

    def save_log(self):
        log = Log()
        log.iteration = self.count_iteration()
        log.time = self.updated_at
        log.accuracy = self.accuracy
        log.min_confidence = self.min_confidence
        log.mean_confidence = self.mean_confidence
        log.stdev_confidence = self.stdev_confidence
        log.selected_docs = self.selected_docs
        self.logs.append(log)
        self.save()
