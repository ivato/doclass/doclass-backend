import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())


class Config:
    DEBUG = False
    _PATH = os.getcwd()
    REPOS_DIR = _PATH+os.getenv("REPOS_DIR")
    MONGODB_DATABASE = os.getenv("MONGODB_DATABASE")
    MONGODB_HOST = os.getenv("MONGODB_HOST")
    MONGODB_PORT = os.getenv("MONGODB_PORT")
    MONGODB_USER = os.getenv("MONGODB_USER")
    MONGODB_PASSWORD = os.getenv("MONGODB_PASSWORD")
    REDIS_HOST = os.getenv("REDIS_HOST")
    REDIS_PORT = os.getenv("REDIS_PORT")

class DevelopmentConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    DEBUG = False


class TestingConfig(Config):
    DEBUG = True


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

config_os = os.getenv('DOCLASS_CONFIG')
if config_os is None:
    config_by_name['default'] = Config
else:
    config_by_name['default'] = config_by_name[config_os]
