import os

from app.main import create_app
from app.api import get_api
from flask import session, request
import redis
from flask_kvsession import KVSessionExtension
from simplekv.memory.redisstore import RedisStore
from app.main.config import Config

app = create_app('default')
app.app_context().push()
app_name = os.getenv('FLASK_APP')
app.secret_key = 'xyz'

store = RedisStore(redis.StrictRedis(
    host=Config.REDIS_HOST, port=Config.REDIS_PORT))
KVSessionExtension(store, app)

if __name__ == app_name:
  print('loading api...')
  api = get_api()
  api.init_app(app)

  app.run()


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        req = request.form
        email = req.get('email')
        session['email'] = email
        return email+' ok'
    else:
        return "<html><form method='post'> \
            Email: <input type='text' name='email'> \
            <input type='submit' value='submit'> \
            </form> \
            </html>"
