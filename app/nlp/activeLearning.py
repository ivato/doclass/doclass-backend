from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import SVC
from sklearn.svm import LinearSVC
import matplotlib.pyplot as plt
from alipy import ToolBox
import scipy
import pandas as pd
import numpy as np
from statistics import stdev
import nltk
nltk.download('stopwords')


class ActiveLearning:
    # def __init__(self):
    #    self.test = "Hi from class"

    # def load_data(self):
    # data_label = pd.read_csv('labels-2018-09-01.tsv.gz', sep='\t')
    # data_content = pd.read_csv('pubs-2018-09-01.tsv.gz', sep='\t', names=["id", "content"])
    #labels = data_label.sort_values(by=['id'])['ac'].values.tolist()
    #corpus = data_content.sort_values(by=['id'])['content'].values.tolist()
    # return labels, corpus

    @staticmethod
    def preprocess(labels, corpus):
        pt_stopwords = nltk.corpus.stopwords.words('portuguese')
        vectorizer = CountVectorizer(analyzer='word', stop_words=pt_stopwords)
        countVector = vectorizer.fit_transform(corpus)
        vectorizer2 = TfidfTransformer(norm='l2', use_idf=False)

        X = scipy.sparse.csr_matrix.todense(
            vectorizer2.fit_transform(countVector))
        y = np.array(labels)

        return X, y

    # static spliting for experiments
    @staticmethod
    def data_split(X, y):
        # X and y is not used for this experiment
        train_idx = [446, 79, 427, 432, 245, 135, 406, 326, 104, 2, 210, 96, 136, 407, 386, 298, 331, 388,
                     113, 83, 16, 144, 292, 78, 253, 115, 377, 411, 316, 59, 125, 307, 11, 187, 429, 344,
                     73, 37, 237, 474, 258, 257, 75, 422, 161, 248, 353, 57, 212, 402, 188, 26, 193, 227,
                     459, 352, 25, 468, 167, 414, 479, 410, 229, 448, 190, 175, 149, 256, 220, 173, 333, 197,
                     198, 1, 434, 334, 287, 449, 351, 329, 240, 64, 463, 397, 45, 236, 164, 92, 335, 368,
                     146, 363, 349, 93, 42, 419, 97, 40, 170, 300, 230, 440, 395, 315, 207, 288, 131, 21,
                     178, 304, 50, 367, 276, 77, 376, 267, 313, 180, 265, 438, 30, 218, 346, 296, 317, 155,
                     243, 224, 318, 54, 426, 447, 361, 251, 327, 451, 384, 320, 19, 81, 372, 481, 55, 475,
                     270, 340, 231, 457, 416, 336, 112, 27, 28, 485, 38, 31, 162, 165, 106, 271, 254, 399,
                     114, 370, 252, 325, 382, 453, 282, 301, 169, 66, 290, 109, 293, 141, 120, 99, 3, 53,
                     241, 143, 22, 235, 435, 128, 255, 52, 323, 378, 171, 404, 280, 308, 15, 387, 219, 488,
                     362, 211, 153, 442, 202, 456, 383, 194, 20, 226, 460, 272, 464, 357, 490, 110, 266, 250,
                     189, 118, 185, 24, 123, 29, 319, 452, 138, 0, 470, 431, 354, 390, 132, 405, 484, 36,
                     428, 279, 274, 338, 394, 389, 365, 88, 119, 355, 413, 483, 150, 284, 393, 137, 63, 103,
                     409, 176, 269, 380, 291, 480, 46, 166, 10, 225, 233, 286, 9, 51, 469, 482, 56, 379,
                     297, 39, 425, 356, 228, 295, 108, 221, 60, 492, 182, 17, 13, 424, 486, 242, 134, 102,
                     41, 71, 348, 209, 324, 374, 6, 87, 172, 443, 18, 157, 238, 222, 299, 359, 294, 74,
                     358, 232, 86, 239, 478, 445, 126, 154, 418, 281, 477, 472, 262, 58, 216, 437, 264, 322,
                     259, 364, 360, 381, 247, 343, 305, 345, 285, 455, 67, 116, 396, 156, 85, 341, 401, 314, 454, 371, 350]

        label_ind = [79, 427, 446]

        test_idx = [278, 101, 441, 337, 273, 436, 283, 306, 439, 105, 195, 62, 489, 330, 168, 312, 181, 152,
                    61, 160, 145, 177, 100, 151, 311, 84, 412, 392, 130, 244, 8, 391, 400, 415, 217,  44,
                    191, 122, 68, 433, 107, 32, 201, 98, 223, 199, 7, 129, 179, 184, 139, 268, 458, 148,
                    321, 147, 473,  4, 373, 403, 398, 471, 205, 70, 186, 369, 204, 43, 174, 462, 91, 249,
                    133, 121, 117, 158, 124, 94, 260, 89, 234, 261, 90, 206, 466, 35, 275, 163, 385, 461,
                    423, 476, 366, 332, 444, 347, 49, 342, 33, 215, 142, 328, 467, 310, 65, 339, 72, 192,
                    420, 491, 203, 48, 303, 183, 95, 465, 302, 47, 196, 76, 309, 246, 200, 14, 208, 127,
                    12, 213, 487, 408, 289, 417, 277, 23, 111, 263, 140, 80, 5, 421, 82, 159, 450, 430, 34, 69, 214, 375]

        unlab_ind = [0, 1, 2, 3, 6, 9, 10, 11, 13, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29,
                     30, 31, 36, 37, 38, 39, 40, 41, 42, 45, 46, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 63, 64, 66,
                     67, 71, 73, 74, 75, 77, 78, 81, 83, 85, 86, 87, 88, 92, 93, 96, 97, 99, 102, 103, 104, 106, 108, 109,
                     110, 112, 113, 114, 115, 116, 118, 119, 120, 123, 125, 126, 128, 131, 132, 134, 135, 136, 137, 138, 141,
                     143, 144, 146, 149, 150, 153, 154, 155, 156, 157, 161, 162, 164, 165, 166, 167, 169, 170, 171, 172, 173,
                     175, 176, 178, 180, 182, 185, 187, 188, 189, 190, 193, 194, 197, 198, 202, 207, 209, 210, 211, 212, 216,
                     218, 219, 220, 221, 222, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 235, 236, 237, 238, 239, 240,
                     241, 242, 243, 245, 247, 248, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 262, 264, 265, 266, 267,
                     269, 270, 271, 272, 274, 276, 279, 280, 281, 282, 284, 285, 286, 287, 288, 290, 291, 292, 293, 294, 295,
                     296, 297, 298, 299, 300, 301, 304, 305, 307, 308, 313, 314, 315, 316, 317, 318, 319, 320, 322, 323, 324,
                     325, 326, 327, 329, 331, 333, 334, 335, 336, 338, 340, 341, 343, 344, 345, 346, 348, 349, 350, 351, 352,
                     353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 367, 368, 370, 371, 372, 374, 376, 377,
                     378, 379, 380, 381, 382, 383, 384, 386, 387, 388, 389, 390, 393, 394, 395, 396, 397, 399, 401, 402, 404,
                     405, 406, 407, 409, 410, 411, 413, 414, 416, 418, 419, 422, 424, 425, 426, 428, 429, 431, 432, 434, 435,
                     437, 438, 440, 442, 443, 445, 447, 448, 449, 451, 452, 453, 454, 455, 456, 457, 459, 460, 463, 464, 468,
                     469, 470, 472, 474, 475, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 488, 490, 492]

        return train_idx, test_idx, label_ind, unlab_ind

    @staticmethod
    def get_strategy(X, y, train_idx):
        unc_measure = ['least_confident', 'margin',
                       'entropy', 'distance_to_boundary']
        alibox = ToolBox(X=X, y=y, query_type='AllLabels', saving_path='.')

        # choose the strategy
        unc = alibox.get_query_strategy(
            strategy_name="QueryInstanceUncertainty", measure=unc_measure[0])
        # qbc = alibox.get_query_strategy(strategy_name="QueryInstanceQBC")
        # eer = alibox.get_query_strategy(strategy_name="QueryExpectedErrorReduction")
        # quire = alibox.get_query_strategy(strategy_name="QueryInstanceQUIRE", train_idx=train_idx)
        # density = alibox.get_query_strategy(strategy_name="QueryInstanceGraphDensity", train_idx=train_idx)

        strategy = unc

        return strategy, alibox

    @staticmethod
    def fit_model(X, y, label_ind, test_idx, alibox):
        performance = ['accuracy_score',
                       'zero_one_loss',
                       'roc_auc_score',
                       'get_fps_tps_thresholds',
                       'f1_score',
                       'hamming_loss',
                       'one_error',
                       'coverage_error',
                       'label_ranking_loss',
                       'label_ranking_average_precision_score',
                       'micro_auc_score',
                       'average_precision_score']
        metric = performance[0]
        svc = LinearSVC(random_state=0, tol=1e-5)
        svmprob = SVC(kernel='linear', probability=True)
        model = svmprob
        model.fit(X=X[label_ind], y=y[label_ind])
        pred = model.predict(X[test_idx])
        accuracy = alibox.calc_performance_metric(
            y_true=y[test_idx], y_pred=pred, performance_metric=metric)
        score = model.score(X[test_idx], y[test_idx])
        return accuracy, score, model

    @staticmethod
    def select_doc(label_ind, unlab_ind, strategy):
        select_ind = strategy.select(
            label_index=label_ind, unlabel_index=unlab_ind, batch_size=1)
        label_ind.append(select_ind[0])
        unlab_ind.remove(select_ind[0])
        return select_ind, label_ind, unlab_ind

    @staticmethod
    def predict_class(X, model):
        #distance = model.decision_function(X)
        confidence = model.predict_proba(X)
        return model.predict(X), confidence

    @staticmethod
    def random_doc(label_ind, unlab_ind):
        np.random.seed(0)
        l = len(unlab_ind)
        select_ind = unlab_ind[np.random.randint(0, high=l)]
        label_ind.append(select_ind)
        unlab_ind.remove(select_ind)
        print(select_ind)
        return label_ind, unlab_ind

    @staticmethod
    def confidence(X, model):
        confsum = 0
        confmin = 100
        preds = model.predict(X)
        confidences = model.predict_proba(X)
        conf_samples = []
        for i in range(493):

            confidence = confidences[i][preds[i]]
            confsum = confsum + confidence
            conf_samples.append(confidence)
            if confidence < confmin:
                confmin = confidence
        mean = confsum/493
        return mean, confmin, stdev(conf_samples)
    