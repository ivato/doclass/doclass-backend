import activeLearning as al

def main():
    #Initialize the Class
    ac = al.activeClass()

    #Define the budget
    budget = 10
    
    #Load the data, returns the labels and corpus
    labels, corpus = ac.load_data()

    #Process the data, portuguese stopwords, vectorized/Tfid 
    X, y = ac.data_process(labels,corpus)
    
    #Split the dataset, get the train, test (0.3 of the total), labeled (bootstrap of 3 instances) and unlabeled data. 
    train_idx, test_idx, label_ind, unlab_ind = ac.data_split()

    #Select random instance
    #ac.random_doc(label_ind, unlab_ind)

    #Define the strategy
    strategy, alibox = ac.get_strategy(X, y, train_idx)
    
    #Fit the model
    accuracy, score, model = ac.fit_model(X, y, label_ind, test_idx, alibox)
    print(accuracy)

    for i in range(budget):
        #Select the next document
        select_ind, label_ind, unlab_ind = ac.select_doc(label_ind, unlab_ind, strategy)
    
        print(select_ind)
        print(label_ind)

        #Re-train the model    
        accuracy, score, model = ac.fit_model(X, y, label_ind, test_idx, alibox)
        print(accuracy)
        break
    
    #Display the selected instance 
    #print(corpus[select_ind[0]])

    #Get the label and the certainty for a specific instance
    for ind in range(493):
        class_of_instance, certainty = ac.predict_class(X[ind], model)
        if class_of_instance[0] == 1:
            print(ind)
            print(class_of_instance[0])
            print(certainty)

if __name__ == "__main__":
    main()
