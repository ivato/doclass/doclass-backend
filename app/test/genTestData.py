from numpy import random
from app.db.doc import Doc, Classification
from app.db.label import Label
from datetime import datetime

l2 = Label.get_by_id(2) # ac.h.y
l3 = Label.get_by_id(3) # ac.h.n
l7 = Label.get_by_id(7) # ac.h.u
lh =  [l2 , l3, l7]
lhp = [0.55, 0.35, 0.1]

l4 = Label.get_by_id(4) # ac.c.y
l5 = Label.get_by_id(5) # ac.c.n
l8 = Label.get_by_id(8) # ac.c.u
lc = [l4, l5, l8]

def gen_random_human_label():
  i = random.choice(3,p=lhp)
  return i

def gen_random_label_index():
  ihl = gen_random_human_label()
  pdif = 0.2
  comp_labels = [0, 1, 2]
  lcp = [0.60, 0.20, 0.20]  
  variate = random.choice([True,False],p=[pdif,1-pdif])
  if variate:
    comp_labels.remove(ihl)
    del lcp[ihl]
    s = lcp[0]+lcp[1]
    p0 = lcp[0]/s
    p1 = lcp[1]/s
    return (ihl, random.choice(comp_labels,p=[p0,p1]))
  else:
    return (ihl,ihl)

def gen_random_label_pair():
  li = gen_random_label_index()
  return (lh[li[0]], lc[li[1]])

def gen_random_label_pair_json():
  li = gen_random_label_index()
  return (lh[li[0]].to_json(), lc[li[1]].to_json())

def gen_classification_pair():
  li = gen_random_label_index()
  values = [1,0,-1]
  rr = 0.3*random.rand()
  rrs = random.choice([-1,1])
  values_a = [values[0]-rr, values[1]+rrs*rr, values[2]+rr]
  lih = lh[li[0]]
  lia = lc[li[1]]
  ch = Classification(
        value=values[li[0]],
        label=lih,
        classifier_type="human",
        classifier_name="akira",
        datetime=datetime.utcnow
      )
  ca = Classification(
        value=values_a[li[1]],
        label=lia,
        classifier_type="algorithm",
        classifier_name="naive-bayes",
        datetime=datetime.utcnow
      )
  return (ch,ca)

def gen_classifications():
  for d in Doc.objects():
    cp = gen_classification_pair()
    d.classifications.append(cp[0])
    d.classifications.append(cp[1])
    d.save()

def correct_labels():
  #ll5 = Label.get_by_id(5) # ac.c.n
  #ll8 = Label.get_by_id(8) # ac.c.u
  ll3 = Label.get_by_id(3) # ac.h.n
  ll7 = Label.get_by_id(7) # ac.h.u
  for d in Doc.objects:
      #av = d.classifications[1].value 
      #al = d.classifications[1].label.full_name 
      #ali = d.classifications[1].label._id
      hv = d.classifications[0].value 
      hl = d.classifications[0].label.full_name 
      hli = d.classifications[0].label._id
      #if ali == 8:
      #  d.classifications[1].label = ll5
      #elif ali == 5:
      #  d.classifications[1].label = ll8
      if hli == 3:
        d.classifications[0].label = ll7
      elif hli == 7:
        d.classifications[0].label = ll3
      d.save()