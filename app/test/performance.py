from app.db.session import Session
from app.db.dataset import Dataset
import sys
import timeit
import random
from statistics import mean

if __name__ == "__main__":
    if len(sys.argv) != 5:
      print('usage: ./'+sys.argv[0]+' <dataset_id> <n_docs> <pred_docs> <n_tests>')
      print('where:')
      print(' dataset_id:  ')
      print(' n_docs: ')
      print(' pred_docs: ')
      print(' n_tests: ')
      sys.exit(1)
    datasetID = int(sys.argv[1])
    select_documents_number = int(sys.argv[2])
    prediction_interval_number = int(sys.argv[3])
    loop = int(sys.argv[4])
    sessionEmail = 'test@email.com'
    load_times = []
    preprocess_times = []
    train_times = []
    prediction_times = []
    preprocess_cache_times = []

    def timeToPrediction(session, docs):
        t1 = timeit.time.time()
        session.predictions(docs)
        t2 = timeit.time.time()
        print('Time to prediction ' + str(len(docs)) + ' docs: ' + str(t2-t1))
        prediction_times.append(t2-t1)
        return t2 - t1

    def timeToSelectDoc(session, docID):
        t1 = timeit.time.time()
        session.select_doc(docID)
        t2 = timeit.time.time()
        return t2 - t1

    dataset = Dataset.get_by_id(datasetID)

    # tests without cache
    for i in range(loop):
      dataset.df_loaded = False
      dataset.is_loaded = False
      dataset.is_preprocessed = False       
      dataset.dataframe()
      dataset.preprocess()
      load_times.append(dataset.df_time)
      preprocess_times.append(dataset.preprocess_time)
      print('\nLoad/preprocessing dataset - experiment '+str(i+1)+ '...')
      print('Preprocess time: '+str(dataset.preprocess_time))
      print('Load dataset time: '+str(dataset.df_time))

    # tests with cache
    for i in range(loop):
        print('\nCreating session ' + str(i+1) + '...')

        # force preprocessing
        dataset.preprocess()
        if dataset.preprocess_cache_hit:
            print('Preprocess load cache time: '+str(dataset.preprocess_cache_time))
            preprocess_cache_times.append(dataset.preprocess_cache_time)

        session = Session()
        session.open(dataset, sessionEmail)

        session_available_docs = session.available_docs
        session_available_docs_ids = [i._id for i in session_available_docs]

        times_to_select_docs = []

        for j in range(select_documents_number):
            ramdomID = session_available_docs_ids[random.randint(
                0, len(session_available_docs_ids)-1)]

            times_to_select_docs.append(timeToSelectDoc(session, ramdomID))

            session_available_docs = session.available_docs
            session_available_docs_ids = [
                i._id for i in session_available_docs]

        average_times_to_select_docs = mean(times_to_select_docs)

        #if select_documents_number > 1:
        print('Average time to train each new document selected: '+str(average_times_to_select_docs))
        train_times.append(average_times_to_select_docs)
        #else:
        #    print('Average time to train each ' + str(select_documents_number) +
        #          ' random selected doc: ' + str(average_times_to_select_docs))

        session_available_docs = session.available_docs
        session_available_docs_ids = [i._id for i in session_available_docs]

        timeToPrediction(
            session, session_available_docs[0: prediction_interval_number])
        
    print('\nFINAL RESULTS:')
    print('average load dataset time: '+str(mean(load_times)))
    print('average preprocess time: '+str(mean(preprocess_times)))
    print('average prediction times: '+str(mean(prediction_times)))
    print('average train times: '+str(mean(train_times)))
    print('average preprocess cache times: '+str(mean(preprocess_cache_times)))

